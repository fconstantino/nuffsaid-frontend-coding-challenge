import { TestingMessageGenerator } from "./api/__mockApi";
import { render } from '@testing-library/react';
import Providers from "./provider";

const api = new TestingMessageGenerator()

export const TestProviders = (props) => (
  <Providers api={api} {...props} />
);

const customRender = (ui, options) =>
  render(ui, { wrapper: TestProviders, ...options });

export * from "@testing-library/react";

export { api as mockedApi }
export { customRender as render };
