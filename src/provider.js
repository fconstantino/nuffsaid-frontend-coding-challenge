
import React from "react";
import { createMuiTheme, CssBaseline, ThemeProvider } from "@material-ui/core";
import { MessageApiContext } from "./api/context";
import Api from "./api";

const darkTheme = createMuiTheme({
  palette: {
    type: "dark",
  },
});

const Providers = ({ children, api = new Api() }) => {
  return (
    <MessageApiContext.Provider value={api}>
      <ThemeProvider theme={darkTheme}>
        <CssBaseline />
        {children}
      </ThemeProvider>
    </MessageApiContext.Provider>
  );
};

export default Providers;
