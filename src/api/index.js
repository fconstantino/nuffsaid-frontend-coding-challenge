import Chance from 'chance'
import lodash from 'lodash'

import { MessagePriority } from '../consts'

class MessageGenerator {

  constructor() {
    this.stopGeneration = true
    this.chance = new Chance()
  }

  setMessageListener = (msgCallback) => {
    this.messageCallback = msgCallback
  }

  stop = () => {
    this.stopGeneration = true
  }

  start = () => {
    // avoid hitting generate() again, if it's already running
    if (!this.isStarted) {
      this.stopGeneration = false
      this.generate()
    }
  }

  get isStarted() {
    return !this.stopGeneration
  }

  newMessage = (priority) => {
    const message = this.chance.string()
    const genPriority = priority ?? this.chance.pickone(MessagePriority.VALUES)
    return { message, priority: genPriority }
  }
  
  generate = () => {
    if (this.stopGeneration || !this.messageCallback) {
      return
    }

    this.messageCallback(this.newMessage())

    const nextInMS = lodash.random(1000, 4000)
    setTimeout(() => {
      this.generate()
    }, nextInMS)
  }
}

export default MessageGenerator
