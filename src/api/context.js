import { createContext, useContext } from "react"

export const MessageApiContext = createContext(null);

export function useMessageApi() {
  return useContext(MessageApiContext);
}
