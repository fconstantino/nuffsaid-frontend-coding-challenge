import MessageGenerator from "."

export class TestingMessageGenerator extends MessageGenerator {

  constructor(...args) {
    super(...args)
    this.prefabMessages = []
  }

  cleanup = () => {
    this.prefabMessages = []
  }

  seedMessages = (times = 10, priority) => {
    for (let i = 0; i < times; i ++) {
      this.prefabMessages.push(this.newMessage(priority))
    }
  }

  // override
  generate = () => {
    if (this.stopGeneration) return;

    // We're sending some prefab messages because we don't want random behaviours on testing
    this.prefabMessages.forEach((msgObj) => this.messageCallback(msgObj))
  }
}