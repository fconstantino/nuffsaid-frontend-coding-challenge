export class MessagePriority {
  static ERROR = 1;
  static WARNING = 2;
  static INFO = 3;

  static VALUES = [
    MessagePriority.ERROR,
    MessagePriority.WARNING,
    MessagePriority.INFO,
  ];
}
