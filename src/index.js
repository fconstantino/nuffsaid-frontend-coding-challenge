import React from "react";
import ReactDOM from "react-dom";
import MessagePanel from "./components/MessagePanel";
import Providers from "./provider";

const NewApp = require("./components/MessagePanel").default;

function renderApp(App) {
  ReactDOM.render(
    <Providers>
      <App />
    </Providers>,
    document.getElementById("root")
  );
}

renderApp(MessagePanel);

if (module.hot) {
  module.hot.accept("./components/MessagePanel", () => {
    renderApp(NewApp);
  });
}
