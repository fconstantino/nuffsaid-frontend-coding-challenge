import { renderHook, act } from "@testing-library/react-hooks";
import useMessageReceiver, { initialState } from "../useMessageReceiver";
import { TestProviders, mockedApi } from "../../testUtils";
import { MessagePriority } from "../../consts";

beforeEach(() => mockedApi.cleanup())

const getHook = (onReceiveMessage) =>
  renderHook(() => useMessageReceiver({
    onReceiveMessage
  }), { wrapper: TestProviders });

test("should begin started", () => {
  const hook = getHook();
  expect(hook.result.current.isRunning).toBe(true);
});

test("should have data by start", async () => {

  mockedApi.seedMessages(3, MessagePriority.INFO);
  mockedApi.seedMessages(1, MessagePriority.ERROR);

  const hook = getHook();

  const messages = hook.result.current.messages;

  expect(messages[MessagePriority.INFO].length).toBe(3);
  expect(messages[MessagePriority.ERROR].length).toBe(1);
});

test("should clean a single message", async () => {

  mockedApi.seedMessages(5, MessagePriority.INFO);
  const toBeRm = mockedApi.prefabMessages[0];

  const hook = getHook();

  act(() => {
    hook.result.current.clear(toBeRm);
  })

  const messages = hook.result.current.messages;

  expect(messages[MessagePriority.INFO].length).toBe(4);
  expect(messages[MessagePriority.INFO].find(el => el === toBeRm.message)).toBeFalsy();
});


test("should clean all messages", async () => {

  mockedApi.seedMessages();

  const hook = getHook();

  act(() => {
    hook.result.current.clearAll();
  })

  const messages = hook.result.current.messages;

  expect(messages).toEqual(initialState);
})

