import { waitFor } from "@testing-library/react";
import { MessagePriority } from "../../consts";
import { render, screen, mockedApi, queryAllByText } from "../../testUtils";
import MessagePanel from '../MessagePanel';

beforeEach(() => mockedApi.cleanup())

test("Expect API to be running and button to be 'Stop' state", () => {
  render(<MessagePanel/>)

  // Button should have Stop text
  expect(screen.queryByText("Stop")).toBeTruthy()
})

test("Push some INFO messages and see if they are there", async () => {
  // generate 5 Info Messages
  mockedApi.seedMessages(5, MessagePriority.INFO)

  render(<MessagePanel/>)

  await waitFor(() => screen.getByText("Count 5"))

  expect(screen.queryAllByText("Notification").length).toBe(5)

  // check if both error list and warning list are empty
  const errorList = screen.getByTestId("message-list:" + MessagePriority.ERROR);
  const warnList = screen.getByTestId("message-list:" + MessagePriority.WARNING);
  
  expect(queryAllByText(errorList, "Notification").length).toBe(0)
  expect(queryAllByText(warnList, "Notification").length).toBe(0)
})