import React from "react";
import PropTypes from "prop-types";
import { MessagePriority } from "../consts";
import { Box, Typography, makeStyles } from "@material-ui/core";
import Message from "./Message";

const messageTypeColors = {
  [MessagePriority.ERROR]: "#F56236",
  [MessagePriority.WARNING]: "#FCE788",
  [MessagePriority.INFO]: "#88FCA3",
};

const messageTypeTitles = {
  [MessagePriority.ERROR]: "Error",
  [MessagePriority.WARNING]: "Warning",
  [MessagePriority.INFO]: "Info",
};

const useStyles = makeStyles({
  messageListContainer: {
    display: "flex",
    flexDirection: "column-reverse",
  },
});

const MessageList = (props) => {
  const { priority, messages } = props;
  React.useDebugValue(messages);

  return (
    <Box data-testid={`message-list:${props.priority}`} display="flex" flexGrow="1" flexDirection="column">
      <Box marginBottom="8px">
        <Typography variant="h5">{`${messageTypeTitles[priority]} Type ${priority}`}</Typography>
        <Typography variant="subtitle1">{`Count ${messages.length}`}</Typography>
      </Box>
      <Box display="flex" flexDirection="column-reverse" gridGap="8px">
        {messages.map((msg) => (
          <Message
            key={`msg:${msg}`}
            color={messageTypeColors[priority]}
            message={msg}
            onClear={() =>
              props.onClearMessage({
                message: msg,
                priority,
              })
            }
          />
        ))}
      </Box>
    </Box>
  );
};

// I would rather use typescript instead to check for prop types, its easier to write
MessageList.propTypes = {
  priority: PropTypes.oneOf(MessagePriority.VALUES).isRequired,
  messages: PropTypes.arrayOf(PropTypes.string).isRequired,
  onClearMessage: PropTypes.func.isRequired,
};

export default MessageList;
