import React from "react";
import PropTypes from "prop-types";
import { Paper, makeStyles, Button, Typography, Grid } from "@material-ui/core";

const useStyles = makeStyles({
  root: (props) => ({
    borderColor: props.color,
    padding: "8px",
  }),
});

const Message = (props) => {
  const styles = useStyles(props);

  return (
    <Paper elevation={2} className={styles.root} variant="outlined">
      <Grid container wrap="nowrap" alignItems="center" justify="space-between" direction="row">
        <Grid container item direction="column">
          <Typography noWrap color="textPrimary">Notification</Typography>
          <Typography noWrap color="textSecondary">{props.message}</Typography>
        </Grid>
        <Grid item xs>
          <Button variant="text" onClick={props.onClear}>
            Clear
          </Button>
        </Grid>
      </Grid>
    </Paper>
  );
};

Message.propTypes = {
  message: PropTypes.string.isRequired,
  onClear: PropTypes.func.isRequired,
  color: PropTypes.string,
}

export default Message;
