import React from "react";
import {
  Button,
  Grid,
  Container,
  Box,
  Snackbar,
  makeStyles,
} from "@material-ui/core";
import { Alert } from "@material-ui/lab";
import lodash from "lodash";
import MessageList from "./MessageList";
import useMessageReceiver from "./useMessageReceiver";
import { MessagePriority } from "../consts";

const useStyles = makeStyles({
  container: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  button: {
    margin: "8px",
  },
});

const MessagePanel = () => {
  const [latestError, setLatestError] = React.useState(null);

  const handleErrorClose = React.useCallback(() => {
    setLatestError(null);
  }, []);

  const onReceiveErrorCallback = React.useCallback(({ priority, message }) => {
    if (priority === MessagePriority.ERROR) {
      setLatestError(message);
    }
  }, []);

  // custom hook to leave the component cleaner
  const {
    isRunning,
    toggleRun,
    clear,
    clearAll,
    messages,
  } = useMessageReceiver({
    onReceiveMessage: onReceiveErrorCallback,
  });

  const styles = useStyles();

  return (
    <Container fixed justify="center">
      <Box
        display="flex"
        marginBottom="16px"
        justifyContent="center"
        flexDirection="row"
      >
        <Button
          className={styles.button}
          variant="contained"
          onClick={toggleRun}
        >
          {isRunning ? "Stop" : "Start"}
        </Button>
        <Button
          className={styles.button}
          variant="contained"
          onClick={clearAll}
        >
          {"Clear"}
        </Button>
      </Box>

      <Grid container item direction="row" xs={12} spacing={4}>
        {lodash.map(messages, (msgList, priority) => (
          <Grid key={`msglist:${priority}`} item sm={4} xs={12}>
            <MessageList
              priority={parseInt(priority)}
              messages={msgList}
              onClearMessage={clear}
            />
          </Grid>
        ))}
      </Grid>

      <Snackbar
        open={!!latestError}
        autoHideDuration={2000}
        onClose={handleErrorClose}
      >
        <Alert
          elevation={6}
          variant="filled"
          onClose={handleErrorClose}
          severity="error"
        >
          {latestError}
        </Alert>
      </Snackbar>
    </Container>
  );
};

export default MessagePanel;
