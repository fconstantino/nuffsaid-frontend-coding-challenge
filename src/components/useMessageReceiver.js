import React from "react";
import { useMessageApi } from "../api/context";
import { MessagePriority } from "../consts";

export const initialState = MessagePriority.VALUES.reduce((obj, priority) => {
  return {
    ...obj,
    [priority]: [],
  };
}, {});

const useMessageReceiver = (
  { onReceiveMessage } = {}
) => {
  const [messages, setMessages] = React.useState(initialState);
  const api = useMessageApi()

  const messageCallback = React.useCallback((msgObj) => {
    const { priority, message } = msgObj;

    onReceiveMessage && onReceiveMessage(msgObj);

    setMessages((state) => ({
      ...state,
      [priority]: [...state[priority], message],
    }));
  }, []);

  const [isRunning, setRunning] = React.useState(api.isStarted);

  const updateRunningState = React.useCallback((shouldRun) => {
    if (shouldRun) {
      api.start();
    } else {
      api.stop();
    }
    setRunning(shouldRun);
  }, [])

  const onToggleRun = () => {
    updateRunningState(!isRunning)
  };

  // auto stop receiving msgs on destroy
  React.useEffect(() => {
    api.setMessageListener(messageCallback);
    updateRunningState(true);
    return () => {
      updateRunningState(false);
    };
  }, []);

  const clear = React.useCallback(({ message, priority }) => {
    if (message && priority) {
      setMessages((state) => ({
        ...state,
        [priority]: state[priority].slice().filter((row) => row !== message),
      }));
    }
  }, []);

  const clearAll = React.useCallback(() => {
    setMessages(initialState);
  }, []);

  return { messages, isRunning, clear, clearAll, toggleRun: onToggleRun };
};

export default useMessageReceiver;
