
[X] Each time a message with the priority level of error is received, a snackbar containing the error message should also appear at the top of the application.

[X] The error should disappear in 2 seconds, when another error message takes its place, or when the user clears it via the provided button located in the error message.


[X] A user should be able to clear all messages at any point.


[X] A user should be able to clear a specific message in a specific column


[X] A user should be able to start and stop incoming messages. By default the messages should be running and displaying on the grid. The start/stop button should update depending on the state of the feed.


[X] A user should see a count of specific messages in each column


[X] Use material-ui components and JSS styles.


[ ] Test your application to the degree that you feel comfortable with. No specific testing frameworks are required.


[X] The UI should be like the example

## Optional

[X] Can you convert these to functional components rather than class based components?